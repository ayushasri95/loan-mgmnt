<?php
class Admin_model extends CI_Model
{

	function can_login($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('admin');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function alluser()
	{
		$this->db->order_by('user_id', 'DESC');
		$result = $this->db->get('registration')->result_array(); // select * from table
		return $result;
	}

	function allmember()
	{
		$this->db->order_by('user_id', 'DESC');
		$result = $this->db->get('membership')->result_array(); // select * from table
		return $result;
	}

	function allexpense()
	{
		$this->db->order_by('user_id', 'DESC');
		$result = $this->db->get('expense')->result_array(); // select * from table
		return $result;
	}

	function get_user($id){
		$this->db->where('id',$id);
		$result = $this->db->get('registration');
		return $result->row_array();
	   }

	   function updateUser($id,$formArray){
		$this->db->where('id',$id);
		$this->db->update('registration',$formArray);
	  }

	  function allLoan()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('loan_mgmt')->result_array(); // select * from table
		return $result;
	}

	function get_loan($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('loan_mgmt')->row_array(); // select * from table
		return $result;
	}

	function update_loan($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('loan_mgmt', $formArray);
	}

	function getAcnt()
	{
		$result = $this->db->get('admin_account')->row_array(); // select * from table
		return $result;
	}

	function insertAcnt($formArray)
	{
		$this->db->insert('admin_account', $formArray);
	}

	function updateAcnt($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('admin_account', $formArray);
	}

	function allContri()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('contributions')->result_array(); // select * from table
		return $result;
	}

	function get_contri($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('contributions')->row_array(); // select * from table
		return $result;
	}

	function update_contri($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('contributions', $formArray);
	}

	function allGallery()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('gallery')->result_array();
		return $result;
	}

	function updateContriUser($user_id, $form)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('registration', $form);
	}

	function allStatement()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('statement')->result_array();
		return $result;
	}

	function getStmnt($id){
		$this->db->where('id',$id);
		return $result = $this->db->get('statement')->row_array(); 
	  }

	  function deleteStmnt($id,$prev_file_path) {
		$this->db->where('id',$id);
		$this->db->delete('statement');
		unlink($prev_file_path); 
	   }

	   function allPayEMI()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('pay_emi')->result_array(); // select * from table
		return $result;
	}

	function update_emi($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('pay_emi', $formArray);
	}

	function get_pay_emi($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('pay_emi')->row_array(); // select * from table
		return $result;
	}

	function allReward()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('rewards')->result_array();
		return $result;
	}
}
