<?php
class Docreg_model extends CI_Model
{

	function can_login($userId, $username, $password)
	{
		$this->db->where('user_id', $userId);
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('registration');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function get_user($id)
	{
		$this->db->where('user_id', $id);
		$result = $this->db->get('registration');
		return $result->row_array();
	}

	function allfamily()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('user_family')->result_array(); // select * from table
		return $result;
	}

	function allLoan()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('loan_mgmt')->result_array(); // select * from table
		return $result;
	}

	function get_loan($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('loan_mgmt')->row_array(); // select * from table
		return $result;
	}

	function update_loan($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('loan_mgmt', $formArray);
	}

	function getAdminAcnt()
	{
		$result = $this->db->get('admin_account')->row_array(); // select * from table
		return $result;
	}

	function allContri()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('contributions')->result_array(); // select * from table
		return $result;
	}

	function get_contri($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('contributions')->row_array(); // select * from table
		return $result;
	}

	function update_contri($id, $formArray)
	{
		$this->db->where('id', $id);
		$this->db->update('contributions', $formArray);
	}

	function allGallery()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('gallery')->result_array();
		return $result;
	}

	function allStatement()
	{
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get('statement')->result_array();
		return $result;
	}

	function allPayEMI()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('pay_emi')->result_array(); // select * from table
		return $result;
	}

	function allReward()
	{
		$this->db->order_by('id', 'DESC');
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('rewards')->result_array();
		return $result;
	}
}
