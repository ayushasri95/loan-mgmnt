<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add Statements</h3>
    </div>
    <?php

    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }

    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Admin/addStatement'; ?>" enctype="multipart/form-data">
       <div class="card-body">

        <div class="form-group">
         <label for="exampleInputEmail1">Title</label>
         <input type="text" class="form-control" value="<?php echo set_value('title'); ?>" id="exampleInputEmail1" name="title" placeholder="Enter Image Title">
		</div>
        

        <div class="form-group">
		 <label for="exampleInputFile">Statement</label>
		 <input type="file" name="statement" class="form-control">
		</div> 

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>