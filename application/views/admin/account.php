<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Account Details</h3>
    </div>
    <?php

     $success = $this->session->userdata('success');
     if($success !="") { 
     echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
     }

    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">

     <?php if(!empty($admacnt)) { ?>
      <form method="post" action="<?php echo base_url() . 'Admin/update_account/'. $admacnt['id']?>" enctype="multipart/form-data">
      <?php } else {?>
      <form method="post" action="<?php echo base_url() . 'Admin/account_details/'?>" enctype="multipart/form-data">
      <?php }?>

       <div class="card-body">
           
           <div class="form-group">
         <label for="exampleInputEmail1">Account Holder Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('holder_name', $admacnt['holder_name']); ?>" id="exampleInputEmail1" name="holder_name">
         <?php //echo form_error('name');
         ?>
         </div>

       <div class="form-group">
         <label for="exampleInputEmail1">Bank Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('bank_name', $admacnt['bank_name']); ?>" id="exampleInputEmail1" name="bank_name">
         <?php //echo form_error('name');
         ?>
    </div>
    
    <div class="form-group">
         <label for="exampleInputEmail1">Account Number</label>
         <input type="text" class="form-control" value="<?php echo set_value('acnt_no', $admacnt['acnt_no']); ?>" id="exampleInputEmail1" name="acnt_no">
        
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">IFSC Code</label>
         <input type="text" class="form-control" value="<?php echo set_value('ifsc_code', $admacnt['ifsc_code']); ?>" id="exampleInputEmail1" name="ifsc_code">
         <?php //echo form_error('name');
         ?>
    </div>
    <div class="form-group">
         <label for="exampleInputEmail1">Branch</label>
         <input type="text" class="form-control" value="<?php echo set_value('branch', $admacnt['branch']); ?>" id="exampleInputEmail1" name="branch">
         <?php //echo form_error('name');
         ?>
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Emergency Fund</label>
         <input type="text" class="form-control" value="<?php echo set_value('emergency_fund', $admacnt['emergency_fund']); ?>" id="exampleInputEmail1" name="emergency_fund" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
       

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">

       <?php if(!empty($admacnt)) { ?>
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Update"> 
        <?php } else {?>

        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save"> <?php }?>

       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>