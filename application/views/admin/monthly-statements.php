<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-9">
            <h1>Monthly Statements</h1>
          </div>
          <div class="col-sm-3">
     <a href="<?php echo site_url('Admin/addStatement'); ?>" class="btn btn-primary" style="margin-left: 70px;">Add Statement</a>
    </div>
       </div>

       
     </div>
      <!-- /.container-fluid -->
    </section>

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Monthly Statements</h3>
    </div>
	<?php
	$success = $this->session->userdata('success');
	if ($success != "") {
	 echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
	}
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }
    ?>
    <div class="row">

     <div class="col-12">
          <div class="card">
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Title</th>
                  
                  <th>Date</th>
                  <th>Statement</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
             <?php $counter2 = 0;
             if (!empty($stmnt)) {
              foreach ($stmnt as $val) {

             
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['title'] ?></td>
                 
                 <td width="5%"><?php echo $val['date'] ?></td>
                 <td width="2%"><a target="_blank" href="<?php echo base_url() . 'uploads/statement/' . $val['statement']; ?>" class="btn btn-success"><i class="fas fa-eye"></i></td>
                 <td width="2%"><a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Admin/deleteStmnt/' . $val['id'] ?>" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                 </tr>

                <?php }
                
                } ?>
                
                
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        </div>
    </div>
   </div>
  </div>
 </section>
</div>