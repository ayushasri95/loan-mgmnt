<div class="content-wrapper">
 <section class="content-header">
  <div class="container-fluid">
   <div class="row">
    <div class="col-sm-10">
     <h1>Manage Contribution</h1>
	</div>
	<div class="col-sm-2">
	<button class="btn btn-primary" id="export-btn"><i class="fa fa-file-excel-o"></i> Export PDF</button></div>
   </div>
  </div>
 </section>
 <?php
 $success = $this->session->userdata('success');
 if ($success != "") {
  echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
 }
 $failure = $this->session->userdata('failure');
 if ($failure != "") {
  echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failure");</script>';
 } ?>

 <div class="col-12 col-sm-6 col-lg-12">
  <div class="card card-primary card-outline card-tabs">
   <div class="card-header p-0 pt-1 border-bottom-0">
    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

     <li class="nav-item">
      <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Approved</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-pending" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Pending</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Rejected</a>
     </li>
    </ul>
   </div>
   <div class="card-body">
    <div class="tab-content" id="custom-tabs-two-tabContent">
     <!-- Men Tab-->
     <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="approveTbl" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              <th>Name</th>
			  <th>Loan Amount</th>
              <th>EMI</th>
              <th>Interest</th>
              <th>Loan Balance</th>
              <th>Contribution</th>
              <th>Total</th>
              
              <th id="status">Status</th>
              <th id="action">Actions</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($contri)) {
              foreach ($contri as $val) {

               if ($val['status'] == 1) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php $query1 = $this->db->get_where('loan_mgmt', array('user_id' => $val['user_id'])) -> row_array(); 
				 
				 echo $query1['amount']?></td></td>
                 <td width="5%"><?php echo $query1['min_emi'] ?></td>
                 <td width="5%"><?php echo (($query1['amount'] * (6/100))/12) ?></td>
				 <td width="5%"><?php $query = $this->db->get_where('registration', array('user_id' => $val['user_id'])) -> row_array();
				 echo $query1['amount'] - $query['total_contri'];
				  ?></td>
                 <td width="5%"><?php echo $val['contribution'] ?></td>
				 <td width="5%"><?php  
				 
				 echo $query['total_contri']?></td>
				 
                 <td width="5%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/edit_contri/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                   </td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>

     <div class="tab-pane fade" id="custom-tabs-two-pending" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
               <th>Sr. No.</th>
              <th>Name</th>
              <th>Amount</th>
              <th>Total</th>
              <th>Status</th>
              <th>Actions</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($contri)) {
              foreach ($contri as $val) {

               if ($val['status'] == 3) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['contribution'] ?></td>
                 <td width="5%"><?php $query = $this->db->get_where('registration', array('user_id' => $val['user_id'])) -> row_array(); 
				 
				 echo $query['total_contri']?></td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 3) { ?>
                   <p class="btn btn-warning">Pending</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/edit_contri/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                  </td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>
     <!-- Women Tab-->
     <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>

        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
             <tr>
               <th>Sr. No.</th>
              <th>Name</th>
			  <th>Loan Amount</th>
              <th>EMI</th>
              <th>Interest</th>
              <th>Loan Balance</th>
              <th>Contribution</th>
              <th>Total</th>
              <th>Status</th>
              <th>Actions</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($contri)) {
              foreach ($contri as $val) {

               if ($val['status'] == 2) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['contribution'] ?></td>
                 <td width="5%"><?php $query = $this->db->get_where('registration', array('user_id' => $val['user_id'])) -> row_array(); 
				 
				 echo $query['total_contri']?></td>>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/edit_contri/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                   </td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>

     </div>

    </div>
   </div>
   <!-- /.card -->
  </div>
 </div>



</div>