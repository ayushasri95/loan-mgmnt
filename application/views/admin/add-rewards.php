<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add Rewards</h3>
    </div>
    <?php

    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }

    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Admin/addReward'; ?>" enctype="multipart/form-data">
       <div class="card-body">

       <div class="form-group">
         <label for="exampleInputEmail1">Username</label>
         <input type="text" class="form-control" value="<?php echo set_value('user_name'); ?>" id="exampleInputEmail1" name="user_name" placeholder="Enter Username">
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">User Id</label>
         <input type="text" class="form-control" value="<?php echo set_value('user_id'); ?>" id="exampleInputEmail1" name="user_id" placeholder="Enter User Id">
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('amount'); ?>" id="exampleInputEmail1" name="amount" placeholder="Enter Amount">
		</div>
        

        <div class="form-group">
         <label for="exampleInputEmail1">Date</label>
         <input type="date" class="form-control" value="<?php echo set_value('date'); ?>" id="exampleInputEmail1" name="date" placeholder="Enter Date">
		</div>
        

        <div class="form-group">
         <label for="exampleInputEmail1">Descrption</label>
         <textarea name="desc" class="form-control" id="" cols="30" rows="3"></textarea>
		</div>
        
        

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>