<div class="content-wrapper">
 <section class="content-header">
  <div class="container-fluid">
   <div class="row">
    <div class="col-sm-10">
     <h1>All Users</h1>
    </div>
    <div class="col-sm-2">
     <a href="<?php echo site_url('Admin/addUser'); ?>" class="btn btn-primary" style="margin-left: 70px;">Add New</a>
    </div>
   </div>
  </div>
 </section>
 <?php
 $success = $this->session->userdata('success');
 if ($success != "") {
  echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
 }
 $failure = $this->session->userdata('failure');
 if ($failure != "") {
  echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failure");</script>';
 } ?>

 <div class="col-12 col-sm-6 col-lg-12">
  <div class="card card-primary card-outline card-tabs">
   <div class="card-header p-0 pt-1 border-bottom-0">
    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

     <li class="nav-item">
      <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Approved</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-pending" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Pending</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Rejected</a>
     </li>
    </ul>
   </div>
   <div class="card-body">
    <div class="tab-content" id="custom-tabs-two-tabContent">
     <!-- Men Tab-->
     <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              <th>User Id</th>
              <th>User Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 1) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['user_id'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['mobile'] ?></td>
                 <td width="5%"><?php echo $val['email'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/editUser/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url(); ?>Admin/" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>

     <div class="tab-pane fade" id="custom-tabs-two-pending" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              <th>User Id</th>
              <th>User Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 3) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['user_id'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['mobile'] ?></td>
                 <td width="5%"><?php echo $val['email'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 3) { ?>
                   <p class="btn btn-warning">Pending</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/editUser/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url(); ?>Admin/" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>
     <!-- Women Tab-->
     <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>

        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              <th>User Id</th>
              <th>User Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 2) {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['user_id'] ?></td>
                 <td width="5%"><?php echo $val['name'] ?></td>
                 <td width="5%"><?php echo $val['mobile'] ?></td>
                 <td width="5%"><?php echo $val['email'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> <a style="margin-right: 5px;" href="<?php echo base_url() . 'Admin/editUser/' . $val['id'] ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url(); ?>Admin/" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>

     </div>

    </div>
   </div>
   <!-- /.card -->
  </div>
 </div>

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Membership Fee Balance</h3>
    </div>
    <div class="row">
	<div class="col-sm-10">
    </div>
    <div class="col-sm-2" style="margin-top: 1%; margin-bottom: 1%;">
     <a href="<?php echo site_url('Admin/addMembership'); ?>" class="btn btn-primary" style="margin-left: 70px;">Add New</a>
    </div>

     <div class="col-12">
          <div class="card">
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Member Name</th>
                  <th>Joining Month</th>
                  <th>Fee</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
             <?php $counter2 = 0;
             if (!empty($member)) {
              foreach ($member as $val) {

             
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['member_name'] ?></td>
                 <td width="5%"><?php echo $val['joining_month'] ?></td>
                 <td width="5%"><?php echo $val['fee'] ?></td>
                 <td width="2%"><a onclick="return confirm('Are you sure?')" href="# ?>" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                 </tr>

                <?php }
                
                } ?>
                
                
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        </div>
    </div>
   </div>
  </div>
 </section>

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Expense Account</h3>
    </div>
    <div class="row">

	<div class="col-sm-10">
    </div>
    <div class="col-sm-2" style="margin-top: 1%; margin-bottom: 1%;">
     <a href="<?php echo site_url('Admin/addExpense'); ?>" class="btn btn-primary" style="margin-left: 70px;">Add New</a>
    </div>

     <div class="col-12">
          <div class="card">
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>User Name</th>
                  <th>Expense Detail</th>
                  <th>Date</th>
                  <th>Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
             <?php $counter2 = 0;
             if (!empty($expense)) {
              foreach ($expense as $val) {

             
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['user_name'] ?></td>
                 <td width="5%"><?php echo $val['detail'] ?></td>
                 <td width="5%"><?php echo $val['date'] ?></td>
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="2%"><a onclick="return confirm('Are you sure?')" href="# ?>" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                 </tr>

                <?php }
                
                } ?>
                
                
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        </div>
    </div>
   </div>
  </div>
 </section>



</div>

