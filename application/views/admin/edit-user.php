<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <?php 
            $success = $this->session->userdata('success');
            if($success !="") { 
            echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
            }
            $failure = $this->session->userdata('failure');
            if($failure !="") { 
            echo '<script>toastr.error("'.$this->session->flashdata('failure').'","failure");</script>';
			  } ?>
			  

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="row">

     <div class="col-md-6">
	 
      <div class="card-header">
       <h3 class="card-title">User Details </h3>
      </div>
      <form method="post" name="createcat" action="<?php echo base_url() . 'Admin/editUser/' . $user_register['id']; ?>" enctype="multipart/form-data">
       <div class="card-body">
		   <div class="form-group">
         <label for="exampleInputEmail1">User Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('username', $user_register['name']); ?>" id="exampleInputEmail1" name="name" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">User Id</label>
         <input type="text" class="form-control" value="<?php echo set_value('user_id', $user_register['user_id']); ?>" id="exampleInputEmail1" name="user_id" readonly>
         <?php //echo form_error('name');
         ?>
		</div>
		<div class="form-group">
         <label for="exampleInputEmail1">User Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('username', $user_register['username']); ?>" id="exampleInputEmail1" name="username" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Password</label>
         <input type="text" class="form-control" value="<?php echo set_value('password', $user_register['password']); ?>" id="exampleInputEmail1" name="password" readonly>
         <?php //echo form_error('name');
         ?>
		</div>
		
        <div class="form-group">
         <label for="exampleInputEmail1">Date of Birth</label>
         <input type="date" class="form-control" value="<?php echo set_value('dob', $user_register['dob']); ?>" id="exampleInputEmail1" name="dob" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Mobile Number</label>
         <input type="text" class="form-control" value="<?php echo set_value('mobile', $user_register['mobile']); ?>" id="exampleInputEmail1" name="mobile" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Whatsapp Number</label>
         <input type="text" class="form-control" value="<?php echo set_value('whatsapp', $user_register['whatsapp']); ?>" id="exampleInputEmail1" name="whatsapp" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Email Address </label>
         <input type="text" class="form-control" value="<?php echo set_value('email', $user_register['email']); ?>" id="exampleInputEmail1" name="email" readonly>
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Address</label>
         <textarea  readonly class="form-control" rows="3" name="address"><?php echo set_value('address', $user_register['address']); ?></textarea>
         <?php //echo form_error('name');
         ?>
        </div>

        <div class="form-group">
        <label>Partner Status</label>
              <select name="status" class="form-control">
               <option value="1" <?php if ($user_register['status'] == 1) {
                                  echo "selected";
                                 } ?>>Approved</option>
               <option value="2" <?php if ($user_register['status'] == 2) {
                                  echo "selected";
                                 } ?>>Rejected</option>
                <option value="3" <?php if ($user_register['status'] == 3) {
                                  echo "selected";
                                 } ?>>Pending</option>
              </select>
         <?php //echo form_error('name');
         ?>
        </div>

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary" name="userSubmit" value="Update">
       </div>
      </form>
     </div>

     <div class="col-md-6">
      <div class="card-header">
       <h3 class="card-title">User Documents </h3>
      </div>


      <div class="row" style="margin-top: 4%;">
        <div class="col-md-12">
            <label>Aadhar Card Front</label>
        </div>
        <div class="col-md-8">
        
        <embed width="100%" height="120px" src="<?php echo base_url() . 'uploads/aadhaar_card_front/' . $user_register['aadhaar_card_front']; ?>" type="">
        
        </div>
        <div class="col-md-4" style="margin-top: 5%;">
        <a href="<?php echo base_url() . 'uploads/aadhaar_card_front/' . $user_register['aadhaar_card_front']; ?>" target="_blank" class="btn btn-primary">View</a> 
        </div>
      </div>

      <div class="row" style="margin-top: 4%;">
        <div class="col-md-12">
            <label>Aadhar Card Back</label>
        </div>
        <div class="col-md-8">
        
        <embed width="100%" height="120px" src="<?php echo base_url() . 'uploads/aadhaar_card_back/' . $user_register['aadhaar_card_back']; ?>" type="">
        
        </div>
        <div class="col-md-4" style="margin-top: 5%;">
        <a href="<?php echo base_url() . 'uploads/aadhaar_card_back/' . $user_register['aadhaar_card_back']; ?>" target="_blank" class="btn btn-primary">View</a> 
        </div>
      </div>

      <div class="row" style="margin-top: 4%;">
        <div class="col-md-12">
            <label>Cheque or Passbook</label>
        </div>
        <div class="col-md-8">
        
        <embed width="100%" height="120px" src="<?php echo base_url() . 'uploads/cheque_passbook/' . $user_register['cheque_passbook']; ?>" type="">
        
        </div>
        <div class="col-md-4" style="margin-top: 5%;">
        <a href="<?php echo base_url() . 'uploads/cheque_passbook/' . $user_register['cheque_passbook']; ?>" target="_blank" class="btn btn-primary">View</a> 
        </div>
      </div>

      <div class="row" style="margin-top: 4%;">
        <div class="col-md-12">
            <label>Pancard</label>
        </div>
        <div class="col-md-8">
        
        <embed width="100%" height="120px" src="<?php echo base_url() . 'uploads/pancard/' . $user_register['pancard']; ?>" type="">
        
        </div>
        <div class="col-md-4" style="margin-top: 5%;">
        <a href="<?php echo base_url() . 'uploads/pancard/' . $user_register['pancard']; ?>" target="_blank" class="btn btn-primary">View</a> 
        </div>
      </div>
      

     
        
        

     </div>
    </div>
   </div>
  </div>
 </section>
</div>