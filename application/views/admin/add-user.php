<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add User</h3>
    </div>
    <?php

    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }

    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Admin/addUser'; ?>" enctype="multipart/form-data">
       <div class="card-body">

        <div class="form-group">
         <label for="exampleInputEmail1">Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('name'); ?>" id="exampleInputEmail1" name="name" placeholder="Enter Name As Per Pan Card">
         <?php echo form_error('name'); ?>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Date of Birth</label>
         <input type="date" class="form-control" value="<?php echo set_value('dob'); ?>" id="exampleInputEmail1" name="dob" placeholder="Enter DOB As Per Pan Card">
        </div>

		<div class="form-group">
         <label for="exampleInputEmail1">Email (Optional)</label>
         <input type="text" class="form-control" value="<?php echo set_value('email'); ?>" id="exampleInputEmail1" name="email" placeholder="Enter Email">
        </div>

		<div class="form-group">
         <label for="exampleInputEmail1">Mobile Number</label>
         <input type="number" class="form-control" value="<?php echo set_value('mobile'); ?>" id="exampleInputEmail1" name="mobile" placeholder="Enter Mobile Number">
        </div>

		<div class="form-group">
         <label for="exampleInputEmail1">Whatsapp Number</label>
         <input type="number" class="form-control" value="<?php echo set_value('whatsapp'); ?>" id="exampleInputEmail1" name="whatsapp" placeholder="Enter Name As Per Pan Card">
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Permanent Address</label>
         <textarea type="number" name="address" class="form-control" placeholder="Permanent Address"></textarea>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Username</label>
         <input type="text" class="form-control" value="<?php echo set_value('username'); ?>" id="exampleInputEmail1" name="username" placeholder="Enter Name As Per Pan Card">
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Password</label>
         <input type="text" class="form-control" value="<?php echo set_value('password'); ?>" id="exampleInputEmail1" name="password" placeholder="Enter Name As Per Pan Card">
        </div>
        

        <div class="form-group">
		 <label for="exampleInputFile">Aadhaar Card (Front Photo)</label>
		 <input type="file" name="aadhaar_card_front" class="form-control">
		</div>
		
		<div class="form-group">
         <label for="exampleInputFile">Aadhaar Card (Back Photo)</label>
         <input type="file" name="aadhaar_card_back" class="form-control">
		</div>
		
		<div class="form-group">
         <label for="exampleInputFile">Cheque or Passbook</label>
         <input type="file" name="cheque_passbook" class="form-control">
		</div>
		
		<div class="form-group">
         <label for="exampleInputFile">Pancard</label>
         <input type="file" name="pancard" class="form-control">
		</div>

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>