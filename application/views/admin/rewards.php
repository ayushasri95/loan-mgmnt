<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-10">
            <h1>Reward Management</h1>
          </div>
          <div class="col-sm-2">
     <a href="<?php echo site_url('Admin/addReward'); ?>" class="btn btn-primary" style="margin-left: 70px;"><i class="fas fa-trophy"> Reward</i></a>
    </div>
       </div>

       
     </div>
      <!-- /.container-fluid -->
    </section>

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Monthly Statements</h3>
    </div>
	<?php
	$success = $this->session->userdata('success');
	if ($success != "") {
	 echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
	}
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }
    ?>
    <div class="row">

     <div class="col-12">
          <div class="card">
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Username</th>
                  <th>Amount</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
             <?php $counter2 = 0;
             if (!empty($rwrd)) {
              foreach ($rwrd as $val) {

             
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 <td width="5%"><?php echo $val['user_id'] ?></td>
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="5%"><?php echo $val['desc'] ?></td>
                 <td width="5%"><?php echo $val['date'] ?></td>
                 <td width="2%"><a onclick="return confirm('Are you sure?')" href="#" class="btn btn-danger"><i class="fas fa-trash"></i></td>
                 </tr>

                <?php }
                
                } ?>
                
                
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        </div>
    </div>
   </div>
  </div>
 </section>
</div>