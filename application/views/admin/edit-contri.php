<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Edit Contribution</h3>
    </div>
    <?php
    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }
    $failure = $this->session->userdata('failure');
    if($failure !="") { 
    echo '<script>toastr.error("'.$this->session->flashdata('failure').'","failure");</script>';
      }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Admin/edit_contri/'. $getcontri['id']; ?>" enctype="multipart/form-data">
       <div class="card-body">

       <div class="form-group">
         <label for="exampleInputEmail1">Your Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('name', $getcontri['name']); ?>" id="exampleInputEmail1" name="name" placeholder="Name of Contributor" readonly>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">User ID</label>
         <input type="text" class="form-control" value="<?php echo set_value('user_id', $getcontri['user_id']); ?>" id="exampleInputEmail1" name="user_id" placeholder="Name of Contributor" readonly>
        </div>

       <div class="form-group">
         <label for="exampleInputEmail1">Monthly Contribution</label>
         <input type="text" class="form-control" value="<?php echo set_value('contribution', $getcontri['contribution']); ?>" id="exampleInputEmail1" name="contribution" placeholder="Loan Amount" readonly>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Interest</label>
         <input type="text" class="form-control" value="<?php echo set_value('interest', $getcontri['interest']); ?>" id="exampleInputEmail1" name="interest" placeholder="Loan Amount" readonly>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Interest</label>
         <input type="text" class="form-control" value="<?php echo set_value('emi_amount', $getcontri['emi_amount']); ?>" id="exampleInputEmail1" name="emi_amount" placeholder="Loan Amount" readonly>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Interest</label>
         <input type="text" class="form-control" value="<?php echo set_value('total', $getcontri['total']); ?>" id="exampleInputEmail1" name="total" placeholder="Loan Amount" readonly>
        </div>

   
        
        <div class="form-group">
        <label>Partner Status</label>
              <select name="status" class="form-control">
               <option value="1" <?php if ($getcontri['status'] == 1) {
                                  echo "selected";
                                 } ?>>Approved</option>
               <option value="2" <?php if ($getcontri['status'] == 2) {
                                  echo "selected";
                                 } ?>>Rejected</option>
                <option value="3" <?php if ($getcontri['status'] == 3) {
                                  echo "selected";
                                 } ?>>Pending</option>
              </select>
         <?php //echo form_error('name');
         ?>
        </div>

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Update">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>