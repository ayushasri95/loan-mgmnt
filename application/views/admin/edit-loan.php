<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Edit Loan</h3>
    </div>
    <?php
    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }
    $failure = $this->session->userdata('failure');
    if($failure !="") { 
    echo '<script>toastr.error("'.$this->session->flashdata('failure').'","failure");</script>';
      }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" action="<?php echo base_url() . 'Admin/editLoan/' . $loan['id']; ?>" enctype="multipart/form-data">
       <div class="card-body">

        <div class="form-group">
         <label for="exampleInputEmail1">Loan Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('loan_name', $loan['loan_name']); ?>" id="exampleInputEmail1" name="loan_name" placeholder="About Loan" readonly>
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('amount', $loan['amount']); ?>" id="amount" name="amount" placeholder="Amount of Loan">
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Duration</label>
         <input type="text" class="form-control" value="<?php echo set_value('duration', $loan['duration']); ?>" id="duration" name="duration" placeholder="Duration of Loan" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">No. of EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('no_of_emi', $loan['no_of_emi']); ?>" id="exampleInputEmail1" name="no_of_emi" placeholder="EMI Free Time" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Minimum EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('min_emi', $loan['min_emi']); ?>" id="min_emi" name="min_emi" placeholder="Minimum EMI" readonly>
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Maximum EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('max_emi', $loan['max_emi']); ?>" id="exampleInputEmail1" name="max_emi" placeholder="Maximum EMI" readonly>
        </div>
        
        <div class="form-group">
        <label>Partner Status</label>
              <select name="status" class="form-control">
               <option value="1" <?php if ($loan['status'] == 1) {
                                  echo "selected";
                                 } ?>>Approved</option>
               <option value="2" <?php if ($loan['status'] == 2) {
                                  echo "selected";
                                 } ?>>Rejected</option>
                <option value="3" <?php if ($loan['status'] == 3) {
                                  echo "selected";
                                 } ?>>Pending</option>
                                 <option value="4" <?php if ($loan['status'] == 4) {
                                  echo "selected";
                                 } ?>>Cancelled</option>
              </select>
         <?php //echo form_error('name');
         ?>
        </div>

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Update">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>