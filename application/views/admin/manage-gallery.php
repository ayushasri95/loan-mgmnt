<div class="content-wrapper">
<?php 
            $success = $this->session->userdata('success');
            if($success !="") { 
            echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
            }
            $failure = $this->session->userdata('failure');
            if($failure !="") { 
            echo '<script>toastr.error("'.$this->session->flashdata('failure').'","failure");</script>';
        } ?>
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-10">
            <h1>Gallery</h1>
          </div>
          
          <div class="col-sm-2">
          <a href="<?php echo site_url('Admin/addGallery'); ?>" class="btn btn-primary" style="margin-left: 70px;">Add New</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        
			  
          
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                  Gallery Images
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                <?php foreach ($gallery as $val) {
				
				?>
                  <div class="col-sm-3">
                    <a href="<?php echo base_url().'uploads/gallery/'.$val['image']; ?>" data-toggle="lightbox" data-title="<?php echo $val['title'];?>" data-gallery="gallery">
                      <h3 style="padding: 10%;"><?php echo $val['title'];?></h3>
                    </a>
				  </div>
				  
				<?php }?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>