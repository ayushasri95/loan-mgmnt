<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Docreg | Admin</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/resources/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/resources/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/resources/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/resources/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
	<div class="login-box" style="width: 600px;">
		<div class="login-logo">
			<a href="#"><b>Docreg Service</b></a>
		</div>
		<!-- /.login-logo -->

		<div class="card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Registration</p>
				<form action="registration" class="login-form" method="post" enctype="multipart/form-data">

					<div class="row">
						<div class="col-md-6">
							<label for="">Name</label>
							<div class="input-group mb-3">
								<input type="text" name="name" class="form-control" placeholder="Name As per Pan Card">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Date of Birth (As per pan card)</label>
							<div class="input-group mb-3">
								<input type="date" name="dob" class="form-control" placeholder="DOB As per Pan Card">
							</div>
						</div>


						<div class="col-md-6">
							<label for="">Email (Optional)</label>
							<div class="input-group mb-3">
								<input type="text" name="email" class="form-control" placeholder="Email">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Mobile Number</label>
							<div class="input-group mb-3">
								<input type="number" name="mobile" class="form-control" placeholder="Mobile Number">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Whatsapp Number</label>
							<div class="input-group mb-3">
								<input type="number" name="whatsapp" class="form-control" placeholder="Whatsapp Number">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Permanent Address</label>
							<div class="input-group mb-3">
								<textarea type="number" name="address" class="form-control" placeholder="Permanent Address"></textarea>
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Username</label>
							<div class="input-group mb-3">
								<input type="name" name="username" class="form-control" placeholder="username">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Password</label>
							<div class="input-group mb-3">
								<input type="password" name="password" class="form-control" placeholder="Password">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Aadhaar Card (Front Photo)</label>
							<div class="input-group mb-3">
								<input type="file" name="aadhaar_card_front" class="form-control" placeholder="Aadhaar Card (Front Photo)">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Aadhaar Card (Back Photo)</label>
							<div class="input-group mb-3">
								<input type="file" name="aadhaar_card_back" class="form-control" placeholder="Aadhaar Card (Back Photo)">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Cancelled Cheque</label>
							<div class="input-group mb-3">
								<input type="file" name="cheque_passbook" class="form-control" placeholder="Cheque or passbook">
							</div>
						</div>

						<div class="col-md-6">
							<label for="">Pancard</label>
							<div class="input-group mb-3">
								<input type="file" name="pancard" class="form-control" placeholder="Pancard">
							</div>
						</div>
					</div>







					<p><input type="checkbox" id="note" name="note"> <b>Note:</b> I agree to join Be Visionary Group and always follow the terms & conditions of the group</p>





					<div class="social-auth-links text-center mb-3">
						<button type="submit" id="signup" class="animated bounceInDown btn btn-primary btn-block" disabled>Sign Up</button>

						<div class="social-auth-links text-center mb-3">
							<a href="../Docreg/">Have an account? Login Here</a>
						</div>
					</div>


				</form>
				<!-- /.social-auth-links -->


			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>/resources/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url(); ?>/resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url(); ?>/resources/dist/js/adminlte.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-toastr/2.1.1/angular-toastr.tpls.min.js"></script>

	<script>
		var checker = document.getElementById('note');
		var sendbtn = document.getElementById('signup');
		checker.onchange = function() {
			sendbtn.disabled = !this.checked;
		};
	</script>

</body>

</html>