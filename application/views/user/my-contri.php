<div class="content-wrapper">
 <section class="content-header">
  <div class="container-fluid">
   <div class="row">
    <div class="col-sm-8">
     <h1>My Contribution</h1>
    </div>
    <div class="col-sm-4">
    
    </div>
   </div>
  </div>
 </section>
 <?php
 $success = $this->session->userdata('success');
 if ($success != "") {
  echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
 }
 $failure = $this->session->userdata('failure');
 if ($failure != "") {
  echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failure");</script>';
 } ?>

<div>
		
		<!-- /.login-logo -->

		<div class="card container">
			<div class="card-body">
				<form action="#" class="login-form" method="post" enctype="multipart/form-data">

					<div class="row">
						<div class="col-md-6">
							<label for="">Be Visionary Member Since</label>
						</div>

						<div class="col-md-6">
                            <p for=""><?php 
                            $user_id = $this->session->userdata('user_id');
                            $this->db->where('user_id', $user_id);
                            $data = $this->db->get('registration')->row_array();
                            echo $data['created_at']; ?></p>
							
						</div>


						<div class="col-md-6">
							<label for="">Contribution Balance Amount</label>
						</div>

						<div class="col-md-6">
							<p for=""><?php echo $data['total_contri']; ?></p>
							
						</div>

						<div class="col-md-6">
							<label for="">Last Contribution Amount</label>
							
						</div>

						<div class="col-md-6">
                            <p for=""><?php 
                            $this->db->where('user_id', $user_id);
                            $this->db->order_by('id', 'DESC');
                            $data2 = $this->db->get('contributions')->row_array();
                            echo $data2['contribution']; ?></p>
							
						</div>

						<div class="col-md-6">
							<label for="">Last Contribution Date</label>
                        </div>
                        
                        <div class="col-md-6">
							<p for=""><?php  echo $data2['add_date']; ?></p>
                        </div>
                        
                        <div class="col-md-6">
							<label for="">Emergency Fund Account Balance</label>
                        </div>
                        
                        <div class="col-md-6">
							<p for=""><?php echo $data['total_contri']; ?></p>
						</div>

					
					</div>







				</form>
				<!-- /.social-auth-links -->


			</div>
			<!-- /.login-card-body -->
		</div>
	</div>


</div>

<style>
.card-body form .row
{
   padding: 3%;
   font-size: 22px;
}

</style>