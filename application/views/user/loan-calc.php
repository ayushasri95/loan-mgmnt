<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Loan Calculator</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post">
       <div class="card-body">

		
		

       <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('amount'); ?>" id="amount" name="amount" placeholder="Amount">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Interest</label>
         <input type="text" class="form-control" value="<?php echo set_value('interest'); ?>" id="interest" name="interest" placeholder="Interest" readonly>
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Duration</label>

         <select name="duration" id="duration" class="form-control" >
             <option value="36">36</option>
             <option id="sixty" value="60">60</option>
         </select>
        </div>
        
        
        
        
        
        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <button type="submit" class="btn btn-primary toastrDefaultSuccess" name="submit" value="submit">Submit</button>
       </div>

       
      </form>

      <p><?php 

            if (isset($_POST['submit'])) {
                $amount = $_POST['amount'];
                $interest = 6;
                $duration = $_POST['duration'];

                $total = (($amount * $interest/100)/$duration);
                
                echo "Calculated Value is: ". '<b>'.round($total, 2).'</b>';
            }


         ?></p>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>


<script>
    document.getElementById('interest').value = '6 %';

    
     
</script>