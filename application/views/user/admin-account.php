<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-10">
            <h1>Account Details</h1>
          </div>
        
       </div>
     </div>
      <!-- /.container-fluid -->
    </section>

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Account Details</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }
    ?>
    <div class="row">

     <div class="col-12">
          <div class="card">
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Account Holder Name</th>
                  <th>Bank Name</th>
                  <th>Account Number</th>
                  <th>IFSC Code</th>
                  <th>Branch</th>
                </tr>
                </thead>
                <tbody>
             
                <tr>
                  <td width="2%"><?php echo 1;?></td>
                  <td width="5%"><?php echo $admacnt['holder_name']?></td>
                  <td width="5%"><?php echo $admacnt['bank_name']?></td>
                  <td width="7%"><?php echo $admacnt['acnt_no']?></td>
                  <td width="5%"><?php echo $admacnt['ifsc_code']?> </td>
                  <td width="5%"><?php echo $admacnt['branch']?> </td>
                 
                </tr>
                
                
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        </div>
    </div>
   </div>
  </div>
 </section>
</div>