<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add Family</h3>
    </div>
    <?php

    $success = $this->session->userdata('success');
    if($success !="") { 
    echo '<script>toastr.success("'.$this->session->flashdata('success').'","Success");</script>';
    }

    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
     
      <form method="post" action="<?php echo base_url() . 'Docreg/addFamily/'?>" enctype="multipart/form-data">
       <div class="card-body">
		   <div class="form-group">
         <label for="exampleInputEmail1">Family Member Name</label>
         <input type="text" name="family_name" class="form-control" placeholder="Enter Family Member Name">
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Relation</label>
         <input type="text" name="family_relation" class="form-control" placeholder="Relation With Member">
         <?php //echo form_error('name');
         ?>
		</div>
		<div class="form-group">
         <label for="exampleInputEmail1">DOB</label>
         <input type="date" name="family_dob" class="form-control" placeholder="Date of Birth">
         <?php //echo form_error('name');
         ?>
        </div>
        <div class="form-group">
         <label for="exampleInputEmail1">Blood Group</label>
         <input type="text" name="family_blood" class="form-control" placeholder="Enter Blood Group">
         <?php //echo form_error('name');
         ?>
		</div>
		
        <div class="form-group">
         <label for="exampleInputEmail1">Photo</label>
         <input type="file" name="family_pic" class="form-control" placeholder="Enter Family Photo">
		</div>
		
		
        


       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>