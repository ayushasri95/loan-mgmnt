<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add Loan</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Docreg/addLoan'; ?>" enctype="multipart/form-data">
       <div class="card-body">

        
         
        <div class="form-group">
         <label for="exampleInputEmail1">Loan Request Amount</label>
         <input type="number" class="form-control" value="<?php echo set_value('amount'); ?>" id="amount" name="amount" placeholder="Amount of Loan">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Type of Loan</label>
         <select name="loan_name" class="form-control" id="loan_name" required>
             <option value="">--Please Select--</option>
             <option value="Normal Loan">Normal Loan</option>
             <option value="Emergency Loan">Emergency Loan</option>
         </select>
		</div>
        
        
        

        <div class="form-group">
         <label for="exampleInputEmail1">Loan Request Date</label>
         <input type="month" class="form-control" value="<?php echo set_value('start_month'); ?>" id="exampleInputEmail1" name="start_month" placeholder="Start Month">
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Duration</label>
         <input type="number" class="form-control" value="<?php echo set_value('duration'); ?>" id="duration" name="duration" placeholder="Duration of Loan">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Interest Rate</label>
         <input type="text" id="interest" class="form-control" value="6 %" placeholder="6 %" disabled> 
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">No. of EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('no_of_emi'); ?>" id="no_of_emi" name="no_of_emi" placeholder="No. of EMI" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Minimum EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('min_emi'); ?>" id="min_emi" name="min_emi" placeholder="Minimum EMI" readonly>
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Maximum EMI</label>
         <input type="number" class="form-control" value="<?php echo set_value('max_emi'); ?>" id="max_emi" name="max_emi" placeholder="Maximum EMI">
         <p id="max_emi_detail"></p>
		</div>

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>

<script type="text/javascript">
document.getElementById('interest').value = '6 %';

<?php 

$user_id = $this->session->userdata('user_id');
$this->db->where('user_id', $user_id);
$this->db->where('status', 1);
$no_of_emi = $this->db->count_all_results('contributions'); ?>

document.getElementById('no_of_emi').value = <?php echo $no_of_emi; ?>;
</script>

