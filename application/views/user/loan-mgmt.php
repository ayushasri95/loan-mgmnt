<div class="content-wrapper">
 <section class="content-header">
  <div class="container-fluid">
   <div class="row">
    <div class="col-sm-8">
     <h1>Manage Loan</h1>
    </div>
    <div class="col-sm-4">
    <button class="btn btn-primary" id="export-userLoan-btn"><i class="fa fa-file-excel-o"></i> Export PDF</button>
     <a href="<?php echo site_url('Docreg/addLoan'); ?>" class="btn btn-primary" style="margin-left: 70px;">Loan Request Form</a>
    </div>
   </div>
  </div>
 </section>
 <?php
 $success = $this->session->userdata('success');
 if ($success != "") {
  echo '<script>toastr.success("' . $this->session->flashdata('success') . '","Success");</script>';
 }
 $failure = $this->session->userdata('failure');
 if ($failure != "") {
  echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failure");</script>';
 } ?>

 <div class="col-12 col-sm-6 col-lg-12">
  <div class="card card-primary card-outline card-tabs">
   <div class="card-header p-0 pt-1 border-bottom-0">
    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

     <li class="nav-item">
      <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Approved</a>
	 </li>
	 <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-emergency" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Emergency Loan</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-pending" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Pending</a>
     </li>
     <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Rejected</a>
	 </li>
	 <li class="nav-item">
      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-cancel" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Cancelled</a>
     </li>
    </ul>
   </div>
   <div class="card-body">
    <div class="tab-content" id="custom-tabs-two-tabContent">
     <!-- Men Tab-->
     <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="loanTbl" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
			  <th>Loan Amount</th>
			  <th>Interest</th>
			  <th>EMI</th>
			  <th>EMI No.</th>
              <th>Loan Balance</th>
              <th>Total EMI + Interest</th>
              <th id="status">Status</th>
              <th id="action">Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 1 && $val['loan_name'] != 'Emergency Loan') {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 
				 <td width="5%"><?php echo $val['amount'] ?></td>
				 <td width="5%"><?php echo "6%" ?></td>
                 <td width="5%"><?php echo $val['min_emi'] ?></td>
				 <td width="5%"><?php echo $val['no_of_emi'] ?> </td>
				 <td width="5%"><?php $query = $this->db->get_where('registration', array('user_id' => $val['user_id'])) -> row_array(); 
				 
				 echo $val['amount'] - $query['total_contri']?> </td>
				 <td width="5%"><?php echo $val['min_emi'] + 6/100?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%">
                 <?php echo '<button class="btn btn-info" data-toggle="modal" data-target="#myModal' . $val['id'] . '"><i class="fas fa-eye"></i></button>' ?>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Docreg/cancelLoan/' . $val['id'] ?>" class="btn btn-warning"><i class="fas fa-window-close"></i></a>
                   <!-- Modal -->
          <div id="myModal<?php echo $val['id']; ?>" class="modal fade" role="dialog">
           <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Loan Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body" style="width: 100%;overflow: auto;">
               
             <div class="row">
              <div class="col-md-8"><strong>Sanctioned Amount</strong> </div>
              <div class="col-md-4"><?php echo $val['amount'] ?></div>
              </div>

               <div class="row">
              <div class="col-md-8"><strong>Monthly EMI</strong> </div>
              <div class="col-md-4"><?php echo $val['min_emi'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Rate of Interest</strong> </div>
              <div class="col-md-4"><?php echo '6 %' ?></div>
              </div>
               
              <div class="row">
              <div class="col-md-8"><strong>EMI Start Date</strong> </div>
              <div class="col-md-4"><?php echo $val['update_date'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Paid EMI</strong> </div>
              <div class="col-md-4"><?php 
                     $user_id = $this->session->userdata('user_id');$this->db->where('user_id', $user_id);
                     $this->db->where('status', 1);
                     $no_of_emi = $this->db->count_all_results('contributions'); 
                     echo $no_of_emi;?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Due EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - ($no_of_emi + 3)) ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Total EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - 3) ?></div>
              </div>





             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
            </div>

           </div>
          </div>
                  
                  </td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>

	 <div class="tab-pane fade" id="custom-tabs-two-emergency" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              
              <th>Loan Amount</th>
              <th>Duration</th>
              <th>No. of EMI</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['loan_name'] == 'Emergency Loan') {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="5%"><?php echo $val['duration'] ?></td>
                 <td width="5%"><?php echo $val['no_of_emi'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 3) { ?>
                   <p class="btn btn-warning">Pending</p>
				  <?php } ?>
				  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-warning">Rejected</p>
				  <?php } ?>
				  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-warning">Cancelled</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> 
             <!-- <a href="<?php echo base_url() . 'Docreg/viewLoan/' . $val['id'] ?>" class="btn btn-info"><i class="fas fa-eye"></i></a> -->
             <?php echo '<button class="btn btn-info" data-toggle="modal" data-target="#myModal' . $val['id'] . '"><i class="fas fa-eye"></i></button>' ?>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Docreg/cancelLoan/' . $val['id'] ?>" class="btn btn-warning"><i class="fas fa-window-close"></i></a>
                  <!-- Modal -->
          <div id="myModal<?php echo $val['id']; ?>" class="modal fade" role="dialog">
           <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Loan Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body" style="width: 100%;overflow: auto;">
               
             <div class="row">
              <div class="col-md-8"><strong>Loan Amount</strong> </div>
              <div class="col-md-4"><?php echo $val['amount'] ?></div>
              </div>

               <div class="row">
              <div class="col-md-8"><strong>Monthly EMI</strong> </div>
              <div class="col-md-4"><?php echo $val['min_emi'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Rate of Interest</strong> </div>
              <div class="col-md-4"><?php echo '6 %' ?></div>
              </div>
               
              <div class="row">
              <div class="col-md-8"><strong>EMI Start Date</strong> </div>
              <div class="col-md-4"><?php echo $val['update_date'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Paid EMI</strong> </div>
              <div class="col-md-4"><?php 
                     $user_id = $this->session->userdata('user_id');$this->db->where('user_id', $user_id);
                     $this->db->where('status', 1);
                     $no_of_emi = $this->db->count_all_results('contributions'); 
                     echo $no_of_emi;?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Due EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - ($no_of_emi + 3)) ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Total EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - 3) ?></div>
              </div>





             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
            </div>

           </div>
          </div>
                  
                  </td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
	 </div>
	 
     <div class="tab-pane fade" id="custom-tabs-two-pending" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              
              <th>Loan Amount</th>
              <th>Duration</th>
              <th>No. of EMI</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 3 && $val['loan_name'] != 'Emergency Loan') {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="5%"><?php echo $val['duration'] ?></td>
                 <td width="5%"><?php echo $val['no_of_emi'] ?> </td>
                 <td width="5%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 3) { ?>
                   <p class="btn btn-warning">Pending</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> 
                 <?php echo '<button class="btn btn-info" data-toggle="modal" data-target="#myModal' . $val['id'] . '"><i class="fas fa-eye"></i></button>' ?>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Docreg/cancelLoan/' . $val['id'] ?>" class="btn btn-warning"><i class="fas fa-window-close"></i></a>
                  
                  <!-- Modal -->
          <div id="myModal<?php echo $val['id']; ?>" class="modal fade" role="dialog">
           <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Loan Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body" style="width: 100%;overflow: auto;">
               
             <div class="row">
              <div class="col-md-8"><strong>Loan Amount</strong> </div>
              <div class="col-md-4"><?php echo $val['amount'] ?></div>
              </div>

               <div class="row">
              <div class="col-md-8"><strong>Monthly EMI</strong> </div>
              <div class="col-md-4"><?php echo $val['min_emi'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Rate of Interest</strong> </div>
              <div class="col-md-4"><?php echo '6 %' ?></div>
              </div>
               
              <div class="row">
              <div class="col-md-8"><strong>EMI Start Date</strong> </div>
              <div class="col-md-4"><?php echo $val['update_date'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Paid EMI</strong> </div>
              <div class="col-md-4"><?php 
                     $user_id = $this->session->userdata('user_id');$this->db->where('user_id', $user_id);
                     $this->db->where('status', 1);
                     $no_of_emi = $this->db->count_all_results('contributions'); 
                     echo $no_of_emi;?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Due EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - ($no_of_emi + 3)) ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Total EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - 3) ?></div>
              </div>





             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
            </div>

           </div>
          </div></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>
     <!-- Women Tab-->
     <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>

        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              
              <th>Loan Amount</th>
              <th>Duration</th>
              <th>No. of EMI</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 2 && $val['loan_name'] != 'Emergency Loan') {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="5%"><?php echo $val['duration'] ?></td>
                 <td width="5%"><?php echo $val['no_of_emi'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 2) { ?>
                   <p class="btn btn-danger">Inactive</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> 
                 <?php echo '<button class="btn btn-info" data-toggle="modal" data-target="#myModal' . $val['id'] . '"><i class="fas fa-eye"></i></button>' ?>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Docreg/cancelLoan/' . $val['id'] ?>" class="btn btn-warning"><i class="fas fa-window-close"></i></a>
                  
                  <!-- Modal -->
          <div id="myModal<?php echo $val['id']; ?>" class="modal fade" role="dialog">
           <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Loan Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body" style="width: 100%;overflow: auto;">
               
             <div class="row">
              <div class="col-md-8"><strong>Loan Amount</strong> </div>
              <div class="col-md-4"><?php echo $val['amount'] ?></div>
              </div>

               <div class="row">
              <div class="col-md-8"><strong>Monthly EMI</strong> </div>
              <div class="col-md-4"><?php echo $val['min_emi'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Rate of Interest</strong> </div>
              <div class="col-md-4"><?php echo '6 %' ?></div>
              </div>
               
              <div class="row">
              <div class="col-md-8"><strong>EMI Start Date</strong> </div>
              <div class="col-md-4"><?php echo $val['update_date'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Paid EMI</strong> </div>
              <div class="col-md-4"><?php 
                     $user_id = $this->session->userdata('user_id');$this->db->where('user_id', $user_id);
                     $this->db->where('status', 1);
                     $no_of_emi = $this->db->count_all_results('contributions'); 
                     echo $no_of_emi;?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Due EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - ($no_of_emi + 3)) ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Total EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - 3) ?></div>
              </div>





             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
            </div>

           </div>
          </div></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>

	 </div>
	 
	 <div class="tab-pane fade" id="custom-tabs-two-cancel" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
      <section class="content">
       <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-12">
         <div class="card">
          <div class="card-body">
           <table id="example11" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>Sr. No.</th>
              
              <th>Loan Amount</th>
              <th>Duration</th>
              <th>No. of EMI</th>
              <th>Status</th>
              <th>Action</th>
             </tr>
            </thead>
            <tbody>
             <?php $counter2 = 0;
             if (!empty($user)) {
              foreach ($user as $val) {

               if ($val['status'] == 4 && $val['loan_name'] != 'Emergency Loan') {
             ?>
                <tr>
                 <td width="5%"><?php echo ++$counter2; ?></td>
                 
                 <td width="5%"><?php echo $val['amount'] ?></td>
                 <td width="5%"><?php echo $val['duration'] ?></td>
                 <td width="5%"><?php echo $val['no_of_emi'] ?> </td>
                 <td width="7%"><?php if ($val['status'] == 1) { ?>
                   <p class="btn btn-success">Active</p>
                  <?php } ?>
                  <?php if ($val['status'] == 4) { ?>
                   <p class="btn btn-warning">Cancelled</p>
                  <?php } ?>
                 </td>
                 <td width="5%"> 
                 <?php echo '<button class="btn btn-info" data-toggle="modal" data-target="#myModal' . $val['id'] . '"><i class="fas fa-eye"></i></button>' ?>
                   <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() . 'Docreg/cancelLoan/' . $val['id'] ?>" class="btn btn-warning"><i class="fas fa-window-close"></i></a>
                  
                  <!-- Modal -->
          <div id="myModal<?php echo $val['id']; ?>" class="modal fade" role="dialog">
           <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Loan Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body" style="width: 100%;overflow: auto;">
               
             <div class="row">
              <div class="col-md-8"><strong>Loan Amount</strong> </div>
              <div class="col-md-4"><?php echo $val['amount'] ?></div>
              </div>

               <div class="row">
              <div class="col-md-8"><strong>Monthly EMI</strong> </div>
              <div class="col-md-4"><?php echo $val['min_emi'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Rate of Interest</strong> </div>
              <div class="col-md-4"><?php echo '6 %' ?></div>
              </div>
               
              <div class="row">
              <div class="col-md-8"><strong>EMI Start Date</strong> </div>
              <div class="col-md-4"><?php echo $val['update_date'] ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Paid EMI</strong> </div>
              <div class="col-md-4"><?php 
                     $user_id = $this->session->userdata('user_id');$this->db->where('user_id', $user_id);
                     $this->db->where('status', 1);
                     $no_of_emi = $this->db->count_all_results('contributions'); 
                     echo $no_of_emi;?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Due EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - ($no_of_emi + 3)) ?></div>
              </div>

              <div class="row">
              <div class="col-md-8"><strong>Total EMI</strong> </div>
              <div class="col-md-4"><?php echo ($val['duration'] - 3) ?></div>
              </div>





             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
            </div>

           </div>
          </div></td>
                </tr>

             <?php }
              }
             } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
       </div>
      </section>
     </div>

    </div>
   </div>
   <!-- /.card -->
  </div>
 </div>



</div>

<style>
.modal-body .row
{
   padding: 3%;
   font-size: 20px;
}

</style>