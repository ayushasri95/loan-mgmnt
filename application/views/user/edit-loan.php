<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Edit Loan</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" action="#" enctype="multipart/form-data">
       <div class="card-body">

        <div class="form-group">
         <label for="exampleInputEmail1">Loan Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('loan_name', $loan['loan_name']); ?>" id="exampleInputEmail1" name="loan_name" placeholder="About Loan" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Start Month</label>
         <input type="month" class="form-control" value="<?php echo set_value('start_month', $loan['start_month']); ?>" id="exampleInputEmail1" name="start_month" placeholder="Start Month" readonly>
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('amount', $loan['amount']); ?>" id="amount" name="amount" placeholder="Amount of Loan" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Duration</label>
         <input type="text" class="form-control" value="<?php echo set_value('duration', $loan['duration']); ?>" id="duration" name="duration" placeholder="Duration of Loan" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">No. of EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('no_of_emi', $loan['no_of_emi']); ?>" id="no_of_emi" name="no_of_emi" placeholder="No. of EMI" readonly>
        </div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Minimum EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('min_emi', $loan['min_emi']); ?>" id="min_emi" name="min_emi" placeholder="Minimum EMI" readonly>
		</div>
        
        <div class="form-group">
         <label for="exampleInputEmail1">Maximum EMI</label>
         <input type="text" class="form-control" value="<?php echo set_value('max_emi', $loan['max_emi']); ?>" id="max_emi" name="max_emi" placeholder="Maximum EMI" readonly>
		</div>

        

       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>