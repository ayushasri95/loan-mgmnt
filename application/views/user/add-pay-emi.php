<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Pay EMI</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Docreg/addPayEmi'; ?>" enctype="multipart/form-data">
       <div class="card-body">

       <div class="form-group">
         <label for="exampleInputEmail1">Account Number</label>
         <input type="number" class="form-control" value="<?php echo set_value('acnt_no'); ?>" id="acnt_no" name="acnt_no" placeholder="Account Number">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="number" class="form-control" value="<?php echo set_value('amount'); ?>" id="amount" name="amount" placeholder="Amount of Loan">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Date of Payment</label>
         <input type="date" class="form-control" value="<?php echo set_value('date_of_pay'); ?>" id="exampleInputEmail1" name="date_of_pay" placeholder="Date of Payment">
		</div>

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>

