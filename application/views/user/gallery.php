<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Gallery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gallery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                  Gallery Images
                </div>
              </div>
              <div class="card-body">
              <div class="row">
                <?php foreach ($gallery as $val) {
				
				?>
                  <div class="col-sm-3">
                    <a href="<?php echo base_url().'uploads/gallery/'.$val['image']; ?>" data-toggle="lightbox" data-title="<?php echo $val['title'];?>" data-gallery="gallery">
                      <h3 style="padding: 10%;"><?php echo $val['title'];?></h3>
                    </a>
				  </div>
				  
				<?php }?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>