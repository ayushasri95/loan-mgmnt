<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Edit Contribution</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" name="createcat" action="<?php echo base_url() . 'Docreg/editContri/'. $getcontri['id']; ?>" enctype="multipart/form-data">
       <div class="card-body">

       <div class="form-group">
         <label for="exampleInputEmail1">Your Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('name', $getcontri['name']); ?>" id="exampleInputEmail1" name="name" placeholder="Name of Contributor" readonly>
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">User ID</label>
         <input type="text" class="form-control" value="<?php echo set_value('user_id', $getcontri['user_id']); ?>" id="exampleInputEmail1" name="user_id" placeholder="Name of Contributor" readonly>
        </div>

       <div class="form-group">
         <label for="exampleInputEmail1">Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('contribution', $getcontri['contribution']); ?>" id="exampleInputEmail1" name="contribution" placeholder="Loan Amount" readonly>
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Account Number</label>
         <input type="text" class="form-control" value="<?php echo set_value('acnt_no', $getcontri['acnt_no']); ?>" id="exampleInputEmail1" name="acnt_no" placeholder="Contribution Account Number">
		</div>
        
       
        
        <div class="form-group">
         <label for="exampleInputEmail1">Contribution Date</label>
         <input type="date" class="form-control" value="<?php echo set_value('contri_date', $getcontri['contri_date']); ?>" id="exampleInputEmail1" name="contri_date" placeholder="Contribution Date">
		</div>
        
        

        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Update">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>