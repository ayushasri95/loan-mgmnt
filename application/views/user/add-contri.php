<div class="content-wrapper">
 <!-- Content Header (Page header) -->

 <section class="content">
  <div class="card card-primary">
   <div class="container-fluid">
    <div class="card-header">
     <h3 class="card-title">Add Contribution</h3>
    </div>
    <?php
    $failure = $this->session->userdata('failure');
    if ($failure != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('failure') . '","failed");</script>';
    }

    $nopic = $this->session->userdata('nopic');
    if ($nopic != "") {
     echo '<script>toastr.error("' . $this->session->flashdata('nopic') . '","failed");</script>';
    }
    ?>
    <div class="row">
     <div class="col-md-3">
     </div>

     <div class="col-md-6">
      <form method="post" action="<?php echo base_url() . 'Docreg/addContri'; ?>" enctype="multipart/form-data">
       <div class="card-body">

       <div class="form-group">
         <label for="exampleInputEmail1">User Name</label>
         <input type="text" class="form-control" value="<?php echo set_value('name', $getuser['name']); ?>" name="name" placeholder="Name of Contributor" readonly>
		</div>
		

       <div class="form-group">
         <label for="exampleInputEmail1">Monthly Contribution</label>
         <input type="number" class="form-control" value="<?php echo set_value('contribution'); ?>" name="contribution" id="contribution" placeholder="Monthly Contribution">
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Interest</label>
         <input type="number" class="form-control" value="<?php echo set_value('interest'); ?>" name="interest" id="interest" placeholder="Interest">
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">EMI Amount</label>
         <input type="text" class="form-control" value="<?php echo set_value('emi_amount'); ?>" name="emi_amount" id="emi_amount" placeholder="EMI Amount">
		</div>
		
		<div class="form-group">
         <label for="exampleInputEmail1">Total</label>
         <input type="number" class="form-control" value="<?php echo set_value('total'); ?>" name="total" id="total" placeholder="Total">
        </div>


        

		<div class="form-group">
         <label for="exampleInputEmail1">Upload Transaction Slip/Screenshot</label>
         <input type="file" class="form-control" value="<?php echo set_value('pic_file'); ?>" name="pic_file" placeholder="Contribution Image">
		</div>
        
        

       </div>
       <div class="vcard-footer" style="padding: .75rem 1.25rem;">
        <input type="submit" class="btn btn-primary toastrDefaultSuccess" name="userSubmit" value="Save">
       </div>
      </form>
     </div>
     <div class="col-md-3">
     </div>
    </div>
   </div>
  </div>
 </section>
</div>
