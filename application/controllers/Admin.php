<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $this->load->view('admin-login');
    }

    function login_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if ($this->Admin_model->can_login($username, $password)) {
                $session_data = array('username' => $username);
                $this->session->set_userdata($session_data);
                $this->session->set_flashdata('success', 'Login successfully!');
                redirect(base_url() . 'Admin/dashboard'); 
            } else {
                $this->session->set_flashdata('failure', 'Invalid Username OR Password');
                redirect(base_url() . 'Admin/index'); 
            }
        } else {
            $this->session->set_flashdata('failure', 'Invalid Username OR Password');
            redirect(base_url() . 'Admin/index');
        }
    }
    
    public function logout()
     {
          $this->session->unset_userdata('username');
          redirect(base_url() . 'Admin/index');
     }

    public function dashboard()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/dashboard');
        $this->load->view('credit'); 
    }

    public function manage_user()
    {
        $data['user'] = $this->Admin_model->alluser();
        $data['member'] = $this->Admin_model->allmember();
        $data['expense'] = $this->Admin_model->allexpense();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/manage-user', $data);
        
    }

    function editUser($id){
          $this->load->library('upload');

          if($this->input->post())
          {
            $formArray['status']=$this->input->post('status');

            date_default_timezone_set('Asia/Kolkata');
            $formArray['created_at'] = date('Y-m-d');
            
            $this->Admin_model->updateUser($id, $formArray);
              $this->session->set_flashdata('success','Record Updated successfully');
              redirect(base_url().'Admin/manage_user');
          }

          else{
          $data['user_register']= $this->Admin_model->get_user($id);
          $this->load->view('admin/header');
          $this->load->view('admin/edit-user', $data);
          $this->load->view('admin/footer');
          }
    }


    public function addUser() {
		$this->load->library('upload'); 
		 
		
		if($this->input->post())
		{
			$formArray = array();
			
			$usernumrow = $this->db->get('registration')->num_rows();
			$user_num_row = $usernumrow+1;
			$user_id = "USER000".$user_num_row;
					
			$formArray['user_id'] = $user_id;
			$formArray['name'] = $this->input->post('name');
			$formArray['dob'] = $this->input->post('dob');
			$formArray['email'] = $this->input->post('email');
			$formArray['mobile'] = $this->input->post('mobile');
			$formArray['whatsapp'] = $this->input->post('whatsapp');
			$formArray['address'] = $this->input->post('address');
			$formArray['password'] = $this->input->post('password');
			$formArray['username'] = $this->input->post('username');

			$formArray['aadhaar_card_front'] = $this->fileupload('aadhaar_card_front', 'uploads/aadhaar_card_front');

			$formArray['aadhaar_card_back'] = $this->fileupload('aadhaar_card_back', 'uploads/aadhaar_card_back');

			$formArray['cheque_passbook'] = $this->fileupload('cheque_passbook', 'uploads/cheque_passbook');

			$formArray['pancard'] = $this->fileupload('pancard', 'uploads/pancard');

			date_default_timezone_set('Asia/Kolkata');
			$formarray['created_at'] = date('Y-m-d H:i');
			

			$data = $this->db->insert('registration', $formArray);
        	if ($data) {
            $this->session->set_flashdata('success','User Added successfully');
            redirect(base_url("Admin/manage_user"));
        }
			
		}

		else
		{
            $this->load->view('admin/header');
            $this->load->view('admin/footer');
            $this->load->view('admin/add-user');
		}
    } 
    
    
    
    public function manage_loan()
    {
        $data['user'] = $this->Admin_model->allLoan();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/manage-loan', $data);
    }
    
    public function editLoan($id) {
		$this->load->library('upload'); 
		 
		
		if($this->input->post())
		{
			$formArray = array();
			$formArray['status'] = $this->input->post('status');
			$formArray['min_emi'] = $this->input->post('min_emi');
			$formArray['amount'] = $this->input->post('amount');
			
			date_default_timezone_set('Asia/Kolkata');
			$formArray['update_date'] = date('Y-m-d');
			

			$this->Admin_model->update_loan($id, $formArray);
               $this->session->set_flashdata('success', 'Record Updated successfully');
               redirect(base_url() . 'Admin/manage_loan');
		}

		else
		{
			$data['loan'] = $this->Admin_model->get_loan($id);
            $this->load->view('admin/header');
            $this->load->view('admin/footer');
            $this->load->view('admin/edit-loan', $data);
            
		}
    } 
    
    public function account_details()
    {
        if($this->input->post())
		{
			$formArray = array();
			
            $formArray['bank_name'] = $this->input->post('bank_name');
            $formArray['holder_name'] = $this->input->post('holder_name');
            $formArray['acnt_no'] = $this->input->post('acnt_no');
            $formArray['ifsc_code'] = $this->input->post('ifsc_code');
            $formArray['branch'] = $this->input->post('branch');

            $this->Admin_model->insertAcnt($formArray);
               $this->session->set_flashdata('success', 'Record Inserted successfully');
               redirect(base_url() . 'Admin/account_details');
        }

        else
		{
        $data['admacnt'] = $this->Admin_model->getAcnt();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/account', $data);
        
        }
    }

    public function update_account($id)
    {
        $formArray = array();
			
            $formArray['bank_name'] = $this->input->post('bank_name');
            $formArray['holder_name'] = $this->input->post('holder_name');
            $formArray['acnt_no'] = $this->input->post('acnt_no');
            $formArray['ifsc_code'] = $this->input->post('ifsc_code');
            $formArray['branch'] = $this->input->post('branch');

            $this->Admin_model->updateAcnt($id, $formArray);
               $this->session->set_flashdata('success', 'Record Updated successfully');
               redirect(base_url() . 'Admin/account_details');
    }

    public function manage_contributions()
    {
        $data['contri'] = $this->Admin_model->allContri();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/manage-contri', $data);
        
    }
    
    public function edit_contri($id) {
		 
		
		if($this->input->post())
		{
			$formArray = array();
			
			
            $formArray['contribution'] = $this->input->post('contribution');
            
            $user_id = $this->input->post('user_id');
            
            $formArray['status'] = $this->input->post('status');

            if($formArray['status'] == 1)
            {
                $query = $this->db->get_where('registration', array('user_id' => $user_id)) -> row_array();

                
                
                $form['total_contri'] = $query['total_contri'] + $formArray['contribution'];
                
                
                $this->Admin_model->updateContriUser($user_id, $form);
            }
			

			$this->Admin_model->update_contri($id, $formArray);
               $this->session->set_flashdata('success', 'Record Updated successfully');
               redirect(base_url() . 'Admin/manage_contributions');
		}

		else
		{
			$data['getcontri'] = $this->Admin_model->get_contri($id);
            $this->load->view('admin/header');
            $this->load->view('admin/footer');
            $this->load->view('admin/edit-contri', $data);
            
		}
    }
    
    public function manage_gallery()
    {   
        $data['gallery'] = $this->Admin_model->allGallery();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/manage-gallery', $data);
        
    }
    
    
    public function addGallery()
    {
        $this->load->library('upload'); 
		if($this->input->post())
		{
			$formArray = array();
			$formArray['title'] = $this->input->post('title');
			$formArray['image'] = $this->fileupload('image', 'uploads/gallery');
			
			$data = $this->db->insert('gallery', $formArray);
        	if ($data) {
                $this->session->set_flashdata('success', 'Gallery Added successfully');
            redirect(base_url("Admin/manage_gallery"));
            }
		} else {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/add-gallery');
        
        }
    }

    public function monthly_statements()
    {   
        $data['stmnt'] = $this->Admin_model->allStatement();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/monthly-statements', $data);
        
    }

    public function addStatement()
    {
        $this->load->library('upload'); 
		if($this->input->post())
		{
			$formArray = array();
			$formArray['title'] = $this->input->post('title');
            $formArray['statement'] = $this->fileupload('statement', 'uploads/statement');
            
            date_default_timezone_set('Asia/Kolkata');
			$formArray['date'] = date('Y-m-d H:i');
			
			$data = $this->db->insert('statement', $formArray);
        	if ($data) {
                $this->session->set_flashdata('success', 'Statement Added successfully');
            redirect(base_url("Admin/monthly_statements"));
            }
		} else {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/add-statement');
        
        }
    }

    function deleteStmnt($id){
        $val =$this->Admin_model->getStmnt($id);
        $img = $val['statement'];
        $prev_file_path = "uploads/statement/".$img;
        if(empty($val)){
        $this->session->set_flashdata('failure', 'Record not found in database');
        redirect(base_url("Admin/monthly_statements")); }
        $this->Admin_model->deleteStmnt($id,$prev_file_path);
        $this->session->set_flashdata('success', 'Record deleted successfully');
        redirect(base_url("Admin/monthly_statements"));
         } 
         
    public function manage_emi()
    {   
		$data['user'] = $this->Admin_model->allPayEMI();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/manage-emi', $data);
    }
    
    public function editPayEmi($id) {
		 
		
		if($this->input->post())
		{
			$formArray = array();
			$formArray['status'] = $this->input->post('status');
			
			$this->Admin_model->update_emi($id, $formArray);
               $this->session->set_flashdata('success', 'Record Updated successfully');
               redirect(base_url() . 'Admin/manage_emi');
		}

		else
		{
			$data['emi'] = $this->Admin_model->get_pay_emi($id);
            $this->load->view('admin/header');
            $this->load->view('admin/footer');
            $this->load->view('admin/edit-pay-emi', $data);
            
		}
    } 

    public function rewards()
    {   
        $data['rwrd'] = $this->Admin_model->allReward();
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/rewards', $data);
        
    }

    public function addReward()
    {
        $this->load->library('upload'); 
		if($this->input->post())
		{
			$formArray = array();
            $formArray['user_id'] = $this->input->post('user_id');
            $formArray['user_name'] = $this->input->post('user_name');
			$formArray['amount'] = $this->input->post('amount');
			$formArray['date'] = $this->input->post('date');
			$formArray['desc'] = $this->input->post('desc');
            
			
			$data = $this->db->insert('rewards', $formArray);
        	if ($data) {
                $this->session->set_flashdata('success', 'Statement Added successfully');
            redirect(base_url("Admin/rewards"));
            }
		} else {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/add-rewards');
        
        }
    }

    public function addMembership()
    {
		if($this->input->post())
		{
			$formArray = array();
            $formArray['user_id'] = $this->input->post('user_id');
            $formArray['member_name'] = $this->input->post('member_name');
			$formArray['joining_month'] = $this->input->post('joining_month');
			$formArray['fee'] = $this->input->post('fee');
            
			
			$data = $this->db->insert('membership', $formArray);
        	if ($data) {
                $this->session->set_flashdata('success', 'Membership Added successfully');
            redirect(base_url("Admin/manage_user"));
            }
		} else {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/add-membership');
        
        }
    }

    public function addExpense()
    {
		if($this->input->post())
		{
			$formArray = array();
            $formArray['user_id'] = $this->input->post('user_id');
            $formArray['user_name'] = $this->input->post('user_name');
			$formArray['detail'] = $this->input->post('detail');
            $formArray['date'] = $this->input->post('date');
            $formArray['amount'] = $this->input->post('amount');
            
			
			$data = $this->db->insert('expense', $formArray);
        	if ($data) {
                $this->session->set_flashdata('success', 'Membership Added successfully');
            redirect(base_url("Admin/manage_user"));
            }
		} else {
        $this->load->view('admin/header');
        $this->load->view('admin/footer');
        $this->load->view('admin/add-expense');
        
        }
    }

    public function fileupload($fileName, $uploadPath)
	{
		if(!empty($_FILES[$fileName]['name'])){
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			if($this->upload->do_upload($fileName)){
			$uploadData = $this->upload->data();
			$pic_file = $uploadData['file_name'];
			}else{
			$pic_file = ''; 
		}
			 }else{
			$pic_file = ''; 
		}

		return $pic_file;
    }


    

}
