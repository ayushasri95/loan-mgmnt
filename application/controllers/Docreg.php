<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Docreg extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Docreg_model');
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function registration()
	{
		$this->load->library('upload');


		if ($this->input->post()) {
			$formArray = array();

			$usernumrow = $this->db->get('registration')->num_rows();
			$user_num_row = $usernumrow + 1;
			$user_id = "USER000" . $user_num_row;

			$formArray['user_id'] = $user_id;
			$formArray['name'] = $this->input->post('name');
			$formArray['dob'] = $this->input->post('dob');
			$formArray['email'] = $this->input->post('email');
			$formArray['mobile'] = $this->input->post('mobile');
			$formArray['whatsapp'] = $this->input->post('whatsapp');
			$formArray['address'] = $this->input->post('address');
			$formArray['password'] = $this->input->post('password');
			$formArray['username'] = $this->input->post('username');

			$formArray['aadhaar_card_front'] = $this->fileupload('aadhaar_card_front', 'uploads/aadhaar_card_front');

			$formArray['aadhaar_card_back'] = $this->fileupload('aadhaar_card_back', 'uploads/aadhaar_card_back');

			$formArray['cheque_passbook'] = $this->fileupload('cheque_passbook', 'uploads/cheque_passbook');

			$formArray['pancard'] = $this->fileupload('pancard', 'uploads/pancard');

			date_default_timezone_set('Asia/Kolkata');
			$formarray['created_at'] = date('Y-m-d H:i');


			$data = $this->db->insert('registration', $formArray);
			if ($data) {
			    $this->session->set_flashdata('success', 'Registration Completed! Waiting for Approval');
				redirect(base_url("Docreg/index"));
			}
		} else {
			$this->load->view('registration');
		}
	}


	

	function login_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run()) {
			$userId = $this->input->post('user_id');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if ($this->Docreg_model->can_login($userId, $username, $password)) {
				$session_data = array('user_id' => $userId, 'username' => $username);
				$this->session->set_userdata($session_data);
				$this->session->set_flashdata('success', 'Login successfully!');
				redirect(base_url() . 'Docreg/dashboard');  
			} else {
				$this->session->set_flashdata('failure', 'Invalid Username OR Password');
				redirect(base_url() . 'Docreg/index');  
			}
		} else {
			$this->session->set_flashdata('failure', 'Invalid Username OR Password');
			redirect(base_url() . 'Docreg/index');
		}
	}
	
	 // IsNotLogin
	function IsNotLogin()
	{
		$Admin_Login = $this->session->get_userdata('user_name');
		if(empty($Admin_Login['user_id']))
		{	
			$this->session->set_flashdata('message','Login firstly !!');
			redirect(base_url() . 'Docreg/index');
		}
     }
    
    public function logout()
     {
          $this->session->unset_userdata('user_id');
          redirect(base_url() . 'Docreg/index');
     }
     
	public function dashboard()
	{
		$this->load->view('user/header');
		$this->load->view('user/footer');
		$this->load->view('user/dashboard');
		$this->load->view('credit');
		
	}

	public function profile()
	{	
	    $this->IsNotLogin();
		$user_id = $this->session->userdata('user_id');
		$data['user_register']= $this->Docreg_model->get_user($user_id);
		$data['userFam'] = $this->Docreg_model->allfamily();
		$this->load->view('user/header');
		$this->load->view('user/footer');
		$this->load->view('user/profile', $data);
		
	}

	public function addFamily()
	{
			$formArray['user_id'] = $this->session->userdata('user_id');
			$formArray['name'] = $this->input->post('family_name');
			$formArray['relation'] = $this->input->post('family_relation');
			$formArray['dob'] = $this->input->post('family_dob');
			$formArray['blood_group'] = $this->input->post('family_blood');

			$formArray['photo'] = $this->fileupload('family_pic', 'uploads/family_pic');

			$data = $this->db->insert('user_family', $formArray);

			if ($data) {
				$this->session->set_flashdata('success', 'Family has been added');
				redirect(base_url("Docreg/profile"));
			} else {
				$this->session->set_flashdata('failure', 'Some Error Occured');
				redirect(base_url("Docreg/userFamily"));
		}
	}

	public function userFamily()
    {
        $data['user'] = $this->Docreg_model->allfamily();
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/family', $data);
        
	}

	public function loan_mgmt()
    {
        $data['user'] = $this->Docreg_model->allLoan();
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/loan-mgmt', $data);
	}
	
	public function addLoan() {
		$this->load->library('upload'); 
		 
		
		if($this->input->post())
		{
			$formArray = array();
			
			$ran = mt_rand('1000', '30000');
            $loan_id = "LOAN$ran";
					
			$formArray['user_id'] = $this->session->userdata('user_id');
			$formArray['loan_id'] = $loan_id;
			$formArray['loan_name'] = $this->input->post('loan_name');
			$formArray['start_month'] = $this->input->post('start_month');
			$formArray['amount'] = $this->input->post('amount');
			$formArray['duration'] = $this->input->post('duration');
			$formArray['no_of_emi'] = $this->input->post('no_of_emi');
			$formArray['min_emi'] = $this->input->post('min_emi');
			$formArray['max_emi'] = $this->input->post('max_emi');
			
			date_default_timezone_set('Asia/Kolkata');
			$formArray['add_date'] = date('Y-m-d H:i');
			
			$this->db->where('amount', $formArray['amount']);
			$query = $this->db->get('loan_mgmt');
			if ($query->num_rows() > 0){
				$this->session->set_flashdata('failure', 'Loan Amount Already Exists');
            	$this->load->view('user/header');
			$this->load->view('user/footer');
            $this->load->view('user/add-loan');
			}

			else{
				$data = $this->db->insert('loan_mgmt', $formArray);
        	if ($data) {
			$this->session->set_flashdata('success', 'Successfully Applied For Loan! Waiting For Approval');
            redirect(base_url("Docreg/loan_mgmt"));
			}
			else{
				$this->session->set_flashdata('failure', 'Some Error Occured');
				redirect(base_url("Docreg/loan_mgmt"));
			}
			}

			
			
		}

		else
		{
			$this->load->view('user/header');
			$this->load->view('user/footer');
            $this->load->view('user/add-loan');
		}
	} 

	public function viewLoan($id) {
			$data['loan'] = $this->Docreg_model->get_loan($id);
			$this->load->view('user/header');
            $this->load->view('user/edit-loan', $data);
            $this->load->view('user/footer');
	} 
	
	public function account_details()
    {
		$data['admacnt'] = $this->Docreg_model->getAdminAcnt();
        $this->load->view('user/header');
        $this->load->view('user/admin-account', $data);
        $this->load->view('user/footer');
	}
	
	public function cancelLoan($id)
	{
		$status['status'] = 4;
		$this->db->where('id', $id);
		 $this->db->update('loan_mgmt', $status);
		 $this->session->set_flashdata('success', 'Record Updated successfully');
		 redirect(base_url() . 'Docreg/loan_mgmt');
	}
	
	public function cancelContri($id)
	{
		$status['status'] = 4;
		$this->db->where('id', $id);
		 $this->db->update('contributions', $status);
		 $this->session->set_flashdata('success', 'Record Updated successfully');
		 redirect(base_url() . 'Docreg/my_account');
	}
	




	public function fileupload($fileName, $uploadPath)
	{
		if (!empty($_FILES[$fileName]['name'])) {
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'jpg|jpeg|png|pdf|csv';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload($fileName)) {
				$uploadData = $this->upload->data();
				$pic_file = $uploadData['file_name'];
			} else {
				$pic_file = '';
			}
		} else {
			$pic_file = '';
		}

		return $pic_file;
	}

	public function my_account()
    {
        $data['contri'] = $this->Docreg_model->allContri();
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/my-account', $data);
	}

	public function myContri()
    {
        $data['contri'] = $this->Docreg_model->allContri();
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/my-contri', $data);
	}

	public function addContri() {
		$this->load->library('upload'); 
		 
		
		if($this->input->post())
		{
			$formArray = array();
			
			
					
			$formArray['user_id'] = $this->session->userdata('user_id');
			$formArray['name'] = $this->input->post('name');
			$formArray['contribution'] = $this->input->post('contribution');
			$formArray['interest'] = $this->input->post('interest');
			$formArray['emi_amount'] = $this->input->post('emi_amount');
			$formArray['total'] = $this->input->post('total');

			$formArray['pic_file'] = $this->fileupload('pic_file', 'uploads/contri_pic');
			
			date_default_timezone_set('Asia/Kolkata');
			$formArray['add_date'] = date('Y-m-d H:i');
			

			$data = $this->db->insert('contributions', $formArray);
        	if ($data) {
				$this->session->set_flashdata('success', 'Contribution Added Successfully! Waiting For Approval');
            redirect(base_url("Docreg/my_account"));
        	} else {
				$this->session->set_flashdata('failure', 'Some Error Occured');
				redirect(base_url("Docreg/my_account"));
			}
			
		}

		else
		{
			$user_id = $this->session->userdata('user_id');
			$data['getuser'] = $this->Docreg_model->get_user($user_id);
			$this->load->view('user/header');
			$this->load->view('user/footer');
            $this->load->view('user/add-contri', $data);
            
		}
	} 

	public function editContri($id) {
		 
		
		if($this->input->post())
		{
			$formArray = array();
			
			
			$formArray['acnt_no'] = $this->input->post('acnt_no');
			$formArray['contri_date'] = $this->input->post('contri_date');
			$formArray['contribution'] = $this->input->post('contribution');
			

			$this->Docreg_model->update_contri($id, $formArray);
               $this->session->set_flashdata('success', 'Record Updated successfully');
               redirect(base_url() . 'Docreg/my_account');
		}

		else
		{
			$data['getcontri'] = $this->Docreg_model->get_contri($id);
			$this->load->view('user/header');
			$this->load->view('user/footer');
            $this->load->view('user/edit-contri', $data);
            
		}
	}


	public function gallery()
    {
		$data['gallery'] = $this->Docreg_model->allGallery();
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/gallery', $data);
        
	}

	public function monthly_statements()
    {   
        $data['stmnt'] = $this->Docreg_model->allStatement();
        $this->load->view('user/header');
        $this->load->view('user/footer');
        $this->load->view('user/monthly-statements', $data);
        
	}
	
	public function pay_emi()
    {   
		$data['user'] = $this->Docreg_model->allPayEMI();
        $this->load->view('user/header');
        $this->load->view('user/footer');
        $this->load->view('user/pay-emi', $data);
	}

	public function addPayEmi()
    {   
		if($this->input->post()) {
			$formArray = array();
			$formArray['user_id'] = $this->session->userdata('user_id');
			$formArray['acnt_no'] = $this->input->post('acnt_no');
			$formArray['amount'] = $this->input->post('amount');
			$formArray['date_of_pay'] = $this->input->post('date_of_pay');

			$data = $this->db->insert('pay_emi', $formArray);
        	if ($data) {
			$this->session->set_flashdata('success', 'Successfully Applied For Loan! Waiting For Approval');
            redirect(base_url("Docreg/pay_emi"));
			}
			else{
				$this->session->set_flashdata('failure', 'Some Error Occured');
				redirect(base_url("Docreg/pay_emi"));
			}
		} else {
			$this->load->view('user/header');
			$this->load->view('user/footer');
			$this->load->view('user/add-pay-emi');
		}
	}

	public function cancelPayEmi($id)
	{
		$status['status'] = 4;
		$this->db->where('id', $id);
		 $this->db->update('pay_emi', $status);
		 $this->session->set_flashdata('success', 'Record Updated successfully');
		 redirect(base_url() . 'Docreg/pay_emi');
	}

	public function rewards()
    {   
        $data['rwrd'] = $this->Docreg_model->allReward();
        $this->load->view('user/header');
        $this->load->view('user/footer');
        $this->load->view('user/rewards', $data);
	}
	
	public function loan_calculator()
    {   
		$this->load->view('user/header');
		$this->load->view('user/footer');
        $this->load->view('user/loan-calc');
    }
	
}
