-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2020 at 09:55 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `docreg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'docreg', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `whatsapp` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `aadhaar_card_front` varchar(100) NOT NULL,
  `aadhaar_card_back` varchar(100) NOT NULL,
  `cheque_passbook` varchar(100) NOT NULL,
  `pancard` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `status` enum('1','2','3') NOT NULL DEFAULT '3' COMMENT '1=active, 2=inactive, 3=pending',
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `user_id`, `name`, `dob`, `email`, `mobile`, `whatsapp`, `address`, `username`, `password`, `aadhaar_card_front`, `aadhaar_card_back`, `cheque_passbook`, `pancard`, `photo`, `status`, `created_at`) VALUES
(3, 'USER0001', 'Ayush', '2020-07-09', 'photography@123', '4562', '1234', 'Lko', 'ayush95', '12345', 'our_services1.jpg', '21.jpg', '11.jpg', '', 'launching_soon1.jpg', '1', ''),
(4, 'USER0002', 'Navneet', '2020-07-09', 'add@123', '4562', '1234', 'Lko', 'navneet123', '12345', 'our_services1.jpg', '21.jpg', '11.jpg', '', 'launching_soon1.jpg', '2', ''),
(5, 'USER0003', 'Pawan K', '2006-02-08', '', '798585', '1234432', 'nagar', 'photography@123', '12345', 'Coming-Soon-.jpg', 'join_now.jpg', 'ps.jpg', 'service1.png', 'Starting-a-blog.png', '3', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
